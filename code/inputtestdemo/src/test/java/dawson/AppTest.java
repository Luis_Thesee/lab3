package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void test(){
        assertEquals("ho should return five", 5, App.echo(5));
    }

    @Test
    public void test2(){
        assertEquals("should return five", 5, App.oneMore(4));
    }
}
